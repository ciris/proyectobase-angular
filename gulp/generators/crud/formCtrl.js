"use strict";

module.exports = FormNAMECtrl;

FormNAMECtrl.$inject = [ "FILE", "NAMEAPI", "$state", "$stateParams", "$auth",
"Validations" ];
function FormNAMECtrl( FILE, NAMEAPI, $state, $stateParams, $auth, Validations ) {
  var vm = this;
  var permissions = $auth.getPayload().permissions.FILE;
  vm.FILE = FILE;
  vm.FILE.editing = ( $stateParams.edit === "true" );
  vm.save = save;
  vm.edit = edit;
  vm.remove = remove;

  if ( vm.FILE._id ) {
    if ( vm.FILE.editing ) {
      Validations.validate( permissions, "edit" );
    } else {
      Validations.validate( permissions, "read" );
    }
  } else {
    Validations.validate( permissions, "create" );
  }

  function save( validForm ) {
    if ( validForm ) {
      NAMEAPI.save( vm.FILE ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function edit( value ) {
    reload( vm.FILE._id, value );
  }

  function remove() {
    NAMEAPI.remove( vm.FILE ).then( function() {
      $state.go( "^.FILE-list" );
    } );
  }

  function reload( id, editing ) {
    $state.go( $state.current, {id: id, edit: editing}, {reload: true} );
  }
}
