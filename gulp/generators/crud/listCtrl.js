"use strict";

module.exports = ListNAMECtrl;

ListNAMECtrl.$inject = [ "list", "NAMEAPI", "$state", "$stateParams" ];
function ListNAMECtrl( list, NAMEAPI, $state, $stateParams ) {
  var vm = this;
  vm.page = parseInt( $stateParams.page || 0 ) + 1;
  vm.qty = parseInt( $stateParams.qty || 10 );
  vm.list = list;
  vm.remove = remove;
  vm.updatePage = updatePage;

  function remove( FILE ) {
    if ( confirm( "Are you sure you want to remove this NAME?" ) ) {
      NAMEAPI.remove( FILE._id ).then( function() {
        vm.list.docs = _.reject( vm.list.docs, function( elem ) {
          return elem._id === FILE._id;
        } );
        vm.list.counter -= 1;
      } );
    }
  }

  function updatePage( page ) {
    $state.go( $state.current, {page: page, qty: vm.qty}, {reload: false} );
  }
}
