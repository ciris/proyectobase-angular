"use strict";

module.exports = NAMEAPI;

var NAME = require( "../../models/FILE.js" );

NAMEAPI.$inject = [ "urlApi", "$http", "Notifications" ];

function NAMEAPI( urlApi, $http, Notifications ) {
  NAMEAPI.get = get;
  NAMEAPI.list = list;
  NAMEAPI.save = save;
  NAMEAPI.remove = remove;
  return NAMEAPI;

  function get( id ) {
    if ( id ) {
      return $http.get( urlApi + "/api/FILE/" + id ).then( function( resp ) {
        return NAME.loadObj( resp.data );
      } );
    } else {
      return new NAME();
    }
  }

  function list( page, qty ) {
    var params = {
      params: {
        page: page || 0,
        qty: qty || 10
      }
    };
    return $http.get( urlApi + "/api/FILE/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = NAME.loadObj( resp.data.docs );
    }
    return resp.data;
  }

  function err( resp ) {
    Notifications.add( resp.status, "NAME" );
    return [];
  }

  function remove( id ) {
    return $http.delete( urlApi + "/api/FILE/" + id ).then( function( resp ) {
      Notifications.add( resp.status, "NAME" );
      return resp.data;
    }, function( resp ) {
      Notifications.add( resp.status, "NAME" );
      return resp;
    } );
  }

  function save( obj ) {
    if ( obj._id ) {
      return edit( obj );
    } else {
      return create( obj );
    }
  }

  function create( obj ) {
    return $http.post( urlApi + "/api/FILE/", obj ).then( function( resp ) {
      Notifications.add( resp.status, "NAME" );
      return NAME.loadObj( resp.data );
    }, function( resp ) {
      Notifications.add( resp.status, "NAME" );
      return resp;
    } );
  }

  function edit( obj ) {
    return $http.put( urlApi + "/api/FILE/" + obj._id, obj ).then( function( resp ) {
      Notifications.add( resp.status, "NAME" );
      return NAME.loadObj( resp.data );
    }, function( resp ) {
      Notifications.add( resp.status, "NAME" );
      return resp;
    } );
  }
}
