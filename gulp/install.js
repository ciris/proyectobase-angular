"use strict";

var gulp = require( "gulp" );
var install = require( "gulp-install" );

module.exports = deps;

function deps() {
  gulp.src( [ "./bower.json", "./package.json" ] )
  .pipe( install() );
}
