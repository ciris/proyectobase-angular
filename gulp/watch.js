"use strict";

var gulp = require( "gulp" );
var routes = require( "./routes" );

module.exports = watch;

function watch( ) {
  gulp.watch( routes.scripts.watch, [ "js:hint" ] );
  gulp.watch( routes.less.watch, [ "build:less" ] );
  gulp.watch( routes.templates.watch, [ "build:html" ] );
  gulp.watch( routes.resources.main, [ "resources" ] );
}
