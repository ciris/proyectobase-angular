"use strict";

module.exports = FormRoleCtrl;

FormRoleCtrl.$inject = [ "role", "RoleAPI", "$state", "$stateParams" ];
function FormRoleCtrl( role, RoleAPI, $state, $stateParams ) {
  var vm = this;
  vm.role = role;
  vm.role.editing = ( $stateParams.edit === "true" );
  vm.save = save;
  vm.edit = edit;
  vm.remove = remove;

  function save( validForm ) {
    if ( validForm ) {
      RoleAPI.save( vm.role ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function edit( value ) {
    reload( vm.role._id, value );
  }

  function remove() {
    RoleAPI.remove( vm.role ).then( function() {
      $state.go( "^.role-list" );
    } );
  }

  function reload( id, editing ) {
    $state.go( $state.current, {id: id, edit: editing}, {reload: true} );
  }
}
