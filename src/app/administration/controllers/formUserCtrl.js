"use strict";

module.exports = FormUserCtrl;

FormUserCtrl.$inject = [ "user", "UserAPI", "RoleAPI", "$state", "$stateParams", "$auth" ];
function FormUserCtrl( user, UserAPI, RoleAPI, $state, $stateParams, $auth ) {
  var vm = this;
  vm.user = user;
  vm.loggedUserId = $auth.getPayload().sub;
  vm.user.editing = ( $stateParams.edit === "true" );
  vm.save = save;
  vm.edit = edit;
  vm.remove = remove;
  vm.applyPermissions = applyPermissions;
  listRoles();

  function save( validForm ) {
    if ( validForm ) {
      UserAPI.save( vm.user ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function edit( value ) {
    reload( vm.user._id, value );
  }

  function remove() {
    UserAPI.remove( vm.user ).then( function() {
      $state.go( "^.user-list" );
    } );
  }

  function reload( id, editing ) {
    $state.go( $state.current, {id: id, edit: editing}, {reload: true} );
  }

  function listRoles() {
    RoleAPI.list().then( function( resp ) {
      vm.roleList = resp.docs;
    } );
  }

  function applyPermissions( role ) {
    var i = _.findIndex( vm.roleList, function( elem ) {
      return elem.name === role;
    } );
    Object.assign( vm.user.permissions, vm.roleList[i].permissions );
  }
}
