"use strict";

module.exports = ListUserCtrl;

ListUserCtrl.$inject = [ "list", "UserAPI", "$state", "$stateParams", "$auth", "$translate" ];
function ListUserCtrl( list, UserAPI, $state, $stateParams, $auth, $translate ) {
  var vm = this;
  vm.loggedUserId = $auth.getPayload().sub;
  vm.page = parseInt( $stateParams.page || 0 ) + 1;
  vm.qty = parseInt( $stateParams.qty || 10 );
  vm.list = list;
  vm.remove = remove;
  vm.updatePage = updatePage;

  function remove( user ) {
    if ( window.confirm( $translate.instant( "admin.delete.user" ) ) ) {
      UserAPI.remove( user._id ).then( function() {
        vm.list.docs = _.reject( vm.list.docs, function( elem ) {
          return elem._id === user._id;
        } );
        vm.list.counter -= 1;
      } );
    }
  }

  function updatePage( page ) {
    $state.go( $state.current, {page: page, qty: vm.qty}, {reload: false} );
  }
}
