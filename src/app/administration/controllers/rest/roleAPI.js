"use strict";

module.exports = RoleAPI;

var Role = require( "../../models/role.js" );

RoleAPI.$inject = [ "urlApi", "$http", "Notifications" ];

function RoleAPI( urlApi, $http, Notifications ) {
  RoleAPI.get = get;
  RoleAPI.list = list;
  RoleAPI.save = save;
  RoleAPI.remove = remove;
  return RoleAPI;

  function get( id ) {
    if ( id ) {
      return $http.get( urlApi + "/api/role/" + id ).then( function( resp ) {
        return Role.loadObj( resp.data );
      } );
    } else {
      return new Role();
    }
  }

  function list( page, qty ) {
    var params = {
      params: {
        page: page || 0,
        qty: qty || 10
      }
    };
    return $http.get( urlApi + "/api/role/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = Role.loadObj( resp.data.docs );
    }
    return resp.data;
  }

  function err( resp ) {
    Notifications.add( resp.status, "Role" );
    return [];
  }

  function remove( id ) {
    return $http.delete( urlApi + "/api/role/" + id ).then( function( resp ) {
      Notifications.add( resp.status, "Role" );
      return resp.data;
    }, function( resp ) {
      Notifications.add( resp.status, "Role" );
      return resp;
    } );
  }

  function save( obj ) {
    if ( obj._id ) {
      return edit( obj );
    } else {
      return create( obj );
    }
  }

  function create( obj ) {
    return $http.post( urlApi + "/api/role/", obj ).then( function( resp ) {
      Notifications.add( resp.status, "Role" );
      return Role.loadObj( resp.data );
    }, function( resp ) {
      Notifications.add( resp.status, "Role" );
      return resp;
    } );
  }

  function edit( obj ) {
    return $http.put( urlApi + "/api/role/" + obj._id, obj ).then( function( resp ) {
      Notifications.add( resp.status, "Role" );
      return Role.loadObj( resp.data );
    }, function( resp ) {
      Notifications.add( resp.status, "Role" );
      return resp;
    } );
  }
}
