"use strict";

var _name = require( "../../../package.json" ).name;

var mod = angular.module( _name + ".Administration", [] );
mod.config( require( "./routes" ) );

mod.controller( "AdministrationCtrl", require( "./controllers/administrationCtrl.js" ) );

mod.controller( "ListUserCtrl", require( "./controllers/listUserCtrl.js" ) );

mod.controller( "FormUserCtrl", require( "./controllers/formUserCtrl.js" ) );

mod.controller( "ListRoleCtrl", require( "./controllers/listRoleCtrl.js" ) );

mod.controller( "FormRoleCtrl", require( "./controllers/formRoleCtrl.js" ) );

mod.factory( "RoleAPI", require( "./controllers/rest/roleAPI.js" ) );

module.exports = mod;
