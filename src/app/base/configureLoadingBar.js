"use strict";

module.exports = configureLoadingBar;

var ingles = require( "../../resources/i18n/en-US.json" );
var espaniol = require( "../../resources/i18n/es-CR.json" );

function configureLoadingBar( language, cfpLoadingBarProvider ) {
  cfpLoadingBarProvider.includeSpinner = false;

  cfpLoadingBarProvider.loadingBarTemplate = "<div id='loading-bar'>" +
    "<aside class='splash-title'>" +
    "<h1>" + texts( language, "title" ) + "</h1>" +
    "<div class='bar'></div>" +
    "<p>" + texts( language, "loadingbar" ) + "</p>" +
    "<img src='resources/images/loading-bars.svg' " +
    "width='64' height='64' class='animated fadeIn'/>" +
    "</aside>" +
    "</div>";
}

function texts( language, key ) {
  switch ( language ) {
  case "es":
    return espaniol[key];
  default:
    return ingles[key];
  }
}
