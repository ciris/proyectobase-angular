"use strict";

exports.date = date;
exports.percentage = percentage;

function date() {
  var dateFormat = "YYYY-MM-DDTHH:mm:ss.sssZ";
  return filter;

  function filter( date, displayFormat ) {
    if ( unNull( date ) ) {
      return "Undefined";
    }
    var formated = giveFormat( date, displayFormat, dateFormat );
    if ( formated ) {
      return formated;
    }
    return "Invalid date";
  }

  function giveFormat( date, displayFormat, dateFormat ) {
    var instance = instantiate( date, displayFormat );
    if ( validateStringFormat( date ) ) {
      return instance( dateFormat );
    }
    if ( date._isAMomentObject ) {
      return instance( );
    }
  }

  function instantiate( date, displayFormat ) {
    return function( format ) {
      if ( format ) {
        return moment( date, dateFormat ).format( displayFormat );
      } else {
        return moment( date ).format( displayFormat );
      }
    };
  }

  function unNull( obj ) {
    return _.isUndefined( obj ) || _.isNull( obj );
  }

  function validateStringFormat( string ) {
    var regex = /^\d{4}-[0-1]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d{3}Z$/;
    return _.isString( string ) && regex.test( string );
  }
}

percentage.$inject = [ "$filter" ];

function percentage( $filter ) {
  return filter;
  function filter( num, decimals ) {
    if ( isNumber( num ) ) {
      return $filter( "number" )( num * 100, decimals || 2 ) + "%";
    }
    return "Undefined";
  }

  function isNumber( n ) {
    return !isNaN( parseFloat( n ) ) && isFinite( n );
  }
}
