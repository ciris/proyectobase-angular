"use strict";

module.exports = notifications;

notifications.$inject = [ "toastr" ];

function notifications( toastr ) {
  var typesOfNoti = {
    2: toastr.success,
    4: toastr.info,
    5: toastr.error
  };
  notifications.add = add;
  notifications.addCustom = addCustom;
  return notifications;

  function type( status ) {

    return _.get( typesOfNoti, String( status ).substr( 0, 1 ), toastr.warning );
  }

  function add( status, obj ) {
    type( status )( ofStatus( status, obj ) );
  }

  function addCustom( status, message ) {
    type( status )( message );
  }

  function ofStatus( status, obj ) {
    var elem = obj || "document";
    var messages = {
      0: "<strong>0</strong> - There's no communication to the server",
      200: "<strong>200</strong> - Your request has been successful",
      201: "<strong>201</strong> - The element " + elem + " has been successfully saved",
      204: "<strong>204</strong> - The element " + elem + " has been successfully deleted",
      400: "<strong>400</strong> - Missing data required to complete the request",
      403: "<strong>493</strong> - Invalid credentials",
      404: "<strong>404</strong> - " + elem + " not found",
      409: "<strong>409</strong> - " +
       "The action can't execute because it will generate conflict",
      470: "<strong>470</strong> - Identification of " + elem + " is not valid",
      471: "<strong>471</strong> - " + elem + " already exists",
      472: "<strong>472</strong> - Expired credentials",
      473: "<strong>473</strong> - Incorrect credentials",
      500: "<strong>500</strong> - " +
       "There has been an unexpected error, please try again later",
      501: "<strong>501</strong> - The service has not been implemented",
      530: "<strong>530</strong> - We couldn't complete your request because" +
      " It took us too long to process, please try again later"
    };
    return _.get( messages, status, "<strong>?</strong> - Unknow" );
  }
}
