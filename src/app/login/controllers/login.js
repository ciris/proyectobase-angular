"use strict";

module.exports = LoginCtrl;
var User = require( "../models/user.js" );

LoginCtrl.$inject = [ "$auth", "Notifications", "$state", "$sessionStorage", "$localStorage" ];
function LoginCtrl( $auth, Notifications, $state, $sessionStorage, $localStorage ) {
  var vm = this;
  vm.login = login;
  vm.user = {
    remember: true
  };

  function login( form, user ) {
    if ( form.$valid ) {
      $auth.login( user ).then( ok, notify );
    } else {
      return Notifications.addCustom( 400, "Required information missing " );
    }
  }

  function ok( res ) {
    var user = User.loadObj( res.data.user );
    $sessionStorage.user = user;
    if ( vm.user.remember === true ) {
      $localStorage.user = user;
    }
    Notifications.addCustom( 200, "Welcome " + user.name );
    $state.go( "index.main" );
  }

  function notify( resp ) {
    if ( resp.status === -1 ) {
      Notifications.addCustom( 500, "there's no connection to the server" );
    }
    Notifications.addCustom( resp.status, "Invalid credentials" );
  }
}
