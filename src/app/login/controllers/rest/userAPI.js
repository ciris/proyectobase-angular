"use strict";

module.exports = UserAPI;
var User = require( "../../models/user.js" );

UserAPI.$inject = [ "$http", "urlApi", "Notifications" ];
function UserAPI( $http, urlApi, Notifications ) {
  var url = urlApi + "/api/user";
  UserAPI.me = me;
  UserAPI.search = search;
  UserAPI.get = get;
  UserAPI.list = list;
  UserAPI.save = save;
  return UserAPI;

  function me() {
    return $http.get( url + "/me" ).then( User.loadObj, notify );
  }

  function search( text ) {
    var params = {
      params: {
        text: text,
        page: 0,
        qty: 10
      }
    };
    return $http.get( url + "/search/", params ).then( User.loadObj, notify );
  }

  function notify( resp ) {
    console.error( resp );
    Notifications.add( resp.status, "User" );
    return resp.data;
  }

  function get( id ) {
    if ( id ) {
      return $http.get( urlApi + "/api/user/" + id ).then( function( resp ) {
        return User.loadObj( resp.data );
      } );
    } else {
      return new User();
    }
  }

  function list( page, qty ) {
    var params = {
      params: {
        page: page || 0,
        qty: qty || 10
      }
    };
    return $http.get( urlApi + "/api/user/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = User.loadObj( resp.data.docs );
    }
    return resp.data;
  }

  function err( resp ) {
    Notifications.add( resp.status, "User" );
    return [];
  }

  function save( obj ) {
    if ( obj._id ) {
      return edit( obj );
    } else {
      return create( obj );
    }
  }

  function create( obj ) {
    return $http.post( urlApi + "/api/user/", obj ).then( function( resp ) {
      Notifications.add( resp.status, "User" );
      return User.loadObj( resp.data );
    }, function( resp ) {
      Notifications.add( resp.status, "User" );
      return resp;
    } );
  }

  function edit( obj ) {
    return $http.put( urlApi + "/api/user/" + obj._id, obj ).then( function( resp ) {
      Notifications.add( resp.status, "User" );
      return User.loadObj( resp.data );
    }, function( resp ) {
      Notifications.add( resp.status, "User" );
      return resp;
    } );
  }

}
