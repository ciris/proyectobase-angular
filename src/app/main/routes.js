"use strict";

module.exports = routes;

routes.$inject = [ "$stateProvider" ];
function routes( $stateProvider ) {

  $stateProvider.state( "index.main", {
    templateUrl: "main/views/index.html",
    url: "/main",
    controller: "MainCtrl",
    controllerAs: "mod",
    data: {
      title: "routes.main.module",
      icon: "fa-home",
      menu: "routes.main.module"
    }
  } );

}
