"use strict";

var _name = require( "../../../../package.json" ).name;

describe( "FormUserCtrl test", function() {

  beforeEach( angular.mock.module( _name ) );

  describe( "initial parameters", function() {
    it( "Should assign a value to the scope", angular.mock.inject( assignValue ) );
    it( "Should be editing", angular.mock.inject( edition ) );
    it( "Shouldn't be editing", angular.mock.inject( read ) );
  } );

} );

var auth = {
  getPayload: function() {
    return {};
  }
};

function assignValue( $controller ) {
  var value = {name: "Test"};
  var ctrl = $controller( "FormUserCtrl", {user: value, $auth: auth } );
  expect( ctrl.user.name ).toEqual( value.name );
  expect( ctrl.user.editing ).toEqual( false );
}

function edition( $controller ) {
  var ctrl = $controller( "FormUserCtrl", {user: {}, $stateParams: {edit: "true"},
  $auth: auth} );
  expect( ctrl.user.editing ).toEqual( true );
}

function read( $controller ) {
  var ctrl = $controller( "FormUserCtrl", {user: {}, $stateParams: {edit: "false"},
  $auth: auth} );
  expect( ctrl.user.editing ).toEqual( false );
}
