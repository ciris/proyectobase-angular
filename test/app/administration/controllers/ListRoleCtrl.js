"use strict";

var _name = require( "../../../../package.json" ).name;

describe( "ListRoleCtrl tests", function() {

  beforeEach( angular.mock.module( _name ) );

  describe( "Initial parameters", function() {
    it( "Should assign a value to the scope", angular.mock.inject( assignValue ) );
  } );

} );

function assignValue( $controller ) {
  var value = {docs: [], counter: 0};
  var ctrl = $controller( "ListRoleCtrl", { list: value } );
  expect( ctrl.list.docs ).toEqual( value.docs );
  expect( ctrl.page ).toEqual( 1 );
  expect( ctrl.qty ).toEqual( 10 );
}
